credentials-binding:523.vd859a_4b_122e6
display-url-api:2.3.6
jquery3-api:3.6.0-4
gradle:1.38
script-security:1172.v35f6a_0b_8207e
pipeline-groovy-lib:591.v3a_7f422b_d058
ws-cleanup:0.42
jquery:1.12.4-1
cloudbees-bitbucket-branch-source:773.v4b_9b_005b_562b_
blueocean-config:1.25.5
checks-api:1.7.4
font-awesome-api:6.1.1-1
popper2-api:2.11.5-2
ssh-credentials:277.v95c2fec1c047
variant:1.4
blueocean-git-pipeline:1.25.5
workflow-support:820.vd1a_6cc65ef33
scm-api:608.vfa_f971c5a_a_e9
git-parameter:0.9.16
github:1.34.3
bootstrap5-api:5.1.3-7
blueocean-commons:1.25.5
jaxb:2.3.6-1
blueocean-pipeline-api-impl:1.25.5
pipeline-github-lib:36.v4c01db_ca_ed16
sshd:3.237.v883d165a_c1d3
blueocean-rest:1.25.5
pipeline-model-definition:2.2086.v12b_420f036e5
workflow-cps:2692.v76b_089ccd026
handlebars:3.0.8
mailer:414.vcc4c33714601
pipeline-rest-api:2.24
github-branch-source:1628.vb_2f51293cb_78
jackson2-api:2.13.3-285.vc03c0256d517
pipeline-model-extensions:2.2086.v12b_420f036e5
echarts-api:5.3.2-1
blueocean-pipeline-scm-api:1.25.5
sse-gateway:1.25
command-launcher:1.2
branch-api:2.1046.v0ca_37783ecc5
ant:475.vf34069fef73c
workflow-job:1182.v60a_e6279b_579
blueocean-pipeline-editor:1.25.5
okhttp-api:4.9.3-105.vb96869f8ac3a
pam-auth:1.8
resource-disposer:0.19
workflow-scm-step:400.v6b_89a_1317c9a_
workflow-aggregator:581.v0c46fa_697ffd
credentials:1126.ve05618c41e62
blueocean-bitbucket-pipeline:1.25.5
docker-workflow:1.28
pipeline-stage-tags-metadata:2.2086.v12b_420f036e5
durable-task:496.va67c6f9eefa7
blueocean-events:1.25.5
blueocean-autofavorite:1.2.5
github-api:1.303-400.v35c2d8258028
ace-editor:1.1
email-ext:2.88
cloudbees-folder:6.722.v8165b_a_cf25e9
blueocean:1.25.5
blueocean-web:1.25.5
pipeline-build-step:2.18
pipeline-stage-step:293.v200037eefcd5
workflow-cps-global-lib:581.ve633085a_8a_87
git-server:1.11
blueocean-personalization:1.25.5
plugin-util-api:2.17.0
pipeline-model-api:2.2086.v12b_420f036e5
workflow-api:1153.vb_912c0e47fb_a_
antisamy-markup-formatter:2.7
pipeline-stage-view:2.24
docker-commons:1.19
workflow-basic-steps:948.v2c72a_091b_b_68
jenkins-design-language:1.25.5
pipeline-input-step:448.v37cea_9a_10a_70
authentication-tokens:1.4
git-client:3.11.0
git:4.11.3
ldap:2.10
timestamper:1.17
jdk-tool:1.0
jsch:0.1.55.2
momentjs:1.1.1
snakeyaml-api:1.30.1
workflow-multibranch:716.vc692a_e52371b_
bouncycastle-api:2.25
ssh-slaves:1.814.vc82988f54b_10
blueocean-i18n:1.25.5
caffeine-api:2.9.3-65.v6a_47d0f4d1fe
javax-mail-api:1.6.2-6
build-timeout:1.20
blueocean-jwt:1.25.5
apache-httpcomponents-client-4-api:4.5.13-1.0
blueocean-dashboard:1.25.5
blueocean-rest-impl:1.25.5
pipeline-milestone-step:101.vd572fef9d926
matrix-auth:3.1.2
blueocean-display-url:2.4.1
matrix-project:771.v574584b_39e60
workflow-step-api:625.vd896b_f445a_f8
javax-activation-api:1.2.0-3
workflow-durable-task-step:1139.v252a_e12e8463
htmlpublisher:1.30
junit:1.63
plain-credentials:1.8
token-macro:293.v283932a_0a_b_49
pipeline-utility-steps:2.12.1
handy-uri-templates-2-api:2.1.8-22.v77d5b_75e6953
jjwt-api:0.11.2-71.v2722b_b_06a_2a_f
pipeline-graph-analysis:195.v5812d95a_a_2f9
favorite:2.4.1
blueocean-core-js:1.25.5
blueocean-github-pipeline:1.25.5
pubsub-light:1.16
structs:318.va_f3ccb_729b_71
trilead-api:1.57.v6e90e07157e1
generic-webhook-trigger:1.84
jersey2-api:2.35-8
gitlab-api:5.0.1-72.vb_8c272862e86
gitlab-branch-source:625.v85cf3a_400cfe
allure-jenkins-plugin:2.30.2
